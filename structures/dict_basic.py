class MyDict:
    def __init__(self, val):
        self.value = dict(val)

    def sda_add(self, val):
        self.value[val[0]] = val[1]

    def sda_print(self):
        print(self.value)


if __name__ == "__main__":
    sda_dict = MyDict([('NAME', 'Jack'), ('AGE', 21)])
    sda_dict.sda_print()
    sda_dict.sda_add(('CITY', 'Gdansk'))
    sda_dict.sda_print()
